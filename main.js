async function register ({
  registerHook,
  registerSetting,
  settingsManager,
  storageManager,
  videoCategoryManager,
  videoLicenceManager,
  videoLanguageManager
}) {
  registerSetting({
    name: 'custom_page_url_path',
    label: 'Path',
    type: 'input',
    private: false,
    default: ''
  })
  registerSetting({
    name: 'custome_page_markdown',
    label: 'Custome Page Markdown',
    type: 'markdown-enhanced',
    private: false,
    default: ''
  })
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}
